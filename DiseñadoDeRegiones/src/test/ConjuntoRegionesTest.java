package test;

import static org.junit.Assert.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import org.junit.Before;
import org.junit.Test;

import logica.AGM;
import logica.Grafo;
import logica.SepararEnRegiones;

public class ConjuntoRegionesTest {

	private Grafo g;
	private AGM arbol;
	private ArrayList<Set<Integer>> listaDeRegionesConexas;
	private Grafo arbolMinimo;

	@Before
	public void grafo() {

		g = new Grafo(9);
		g.agregarArista(0, 1, 1);
		g.agregarArista(1, 2, 2);
		g.agregarArista(2, 3, 100);
		g.agregarArista(3, 4, 3);
		g.agregarArista(4, 5, 4);
		g.agregarArista(5, 6, 100);
		g.agregarArista(6, 7, 5);
		g.agregarArista(7, 8, 6);

		arbol = new AGM();
		arbolMinimo = arbol.kruskal(g);
	}

	@Test
	public void cantidadRegionesTest() {

		listaDeRegionesConexas = SepararEnRegiones.conexas(arbolMinimo, 3);

		int cantidadRegiones = listaDeRegionesConexas.size();

		assertEquals(3, cantidadRegiones);
	}

	@Test
	public void regionesDisconexasTest() {

		listaDeRegionesConexas= SepararEnRegiones.conexas(arbolMinimo, 3);
		assertEquals(regionesDisconexas(), listaDeRegionesConexas);
	}

	private ArrayList<Set<Integer>> regionesDisconexas() {

		ArrayList<Set<Integer>> conjuntoGenerado = new ArrayList<Set<Integer>>();

		Set<Integer> primerConjunto = new HashSet<Integer>();
		primerConjunto.add(0);
		primerConjunto.add(1);
		primerConjunto.add(2);

		Set<Integer> segundoConjunto = new HashSet<Integer>();
		segundoConjunto.add(3);
		segundoConjunto.add(4);
		segundoConjunto.add(5);

		Set<Integer> tercerConjunto = new HashSet<Integer>();
		tercerConjunto.add(6);
		tercerConjunto.add(7);
		tercerConjunto.add(8);

		conjuntoGenerado.add(primerConjunto);
		conjuntoGenerado.add(segundoConjunto);
		conjuntoGenerado.add(tercerConjunto);

		return conjuntoGenerado;
	}
}
