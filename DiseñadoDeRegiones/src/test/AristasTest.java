package test;

import static org.junit.Assert.*;
import java.util.HashSet;
import java.util.Set;
import org.junit.Test;

import logica.Grafo;

public class AristasTest {

	@Test(expected = IllegalArgumentException.class)
	public void primerverticeNegativoTest() {
		Grafo grafo = new Grafo(5);

		grafo.agregarArista(-1, 3, 10);
	}

	@Test(expected = IllegalArgumentException.class)
	public void primerVerticeExcedidoTest() {
		Grafo grafo = new Grafo(5);

		grafo.agregarArista(5, 2, 10);
	}

	@Test(expected = IllegalArgumentException.class)
	public void segundoVerticeNegativoTest() {
		Grafo grafo = new Grafo(5);

		grafo.agregarArista(3, -1, 10);
	}

	@Test(expected = IllegalArgumentException.class)
	public void segundoVerticeExcedidoTest() {
		Grafo grafo = new Grafo(5);

		grafo.agregarArista(2, 5, 10);
	}

	@Test(expected = IllegalArgumentException.class)
	public void agregarLooopTest() {
		Grafo grafo = new Grafo(5);

		grafo.agregarArista(2, 2, 10);
	}

	@Test(expected = IllegalArgumentException.class)
	public void pesoNegativoTest() {
		Grafo grafo = new Grafo(5);
		grafo.agregarArista(1, 2, -1);
	}

	@Test(expected = IllegalArgumentException.class)
	public void intentarAgregarAristaOpuestaTest() {
		Grafo grafo = new Grafo(5);
		grafo.agregarArista(2, 3, 10);
		grafo.agregarArista(3, 2, 10);
	}

	@Test
	public void aristaExistenteTest() {
		Grafo grafo = new Grafo(5);
		grafo.agregarArista(2, 3, 10);

		assertTrue(grafo.existeArista(2, 3));
	}

	@Test
	public void eliminarAristaExistenteTest() {
		Grafo grafo = new Grafo(5);
		grafo.agregarArista(2, 4, 10);
		grafo.eliminarArista(2, 4);

		assertFalse(grafo.existeArista(2, 4));
	}

	@Test
	public void aristaOpuestaTest() {
		Grafo grafo = new Grafo(5);
		grafo.agregarArista(2, 3, 10);

		assertTrue(grafo.existeArista(3, 2));
	}

	@Test
	public void aristaInexistenteTest() {
		Grafo grafo = new Grafo(5);
		grafo.agregarArista(2, 3, 10);

		assertFalse(grafo.existeArista(1, 4));
	}

	@Test
	public void eliminarAristaInexistenteTest() {
		Grafo grafo = new Grafo(5);
		grafo.eliminarArista(2, 4);

		assertFalse(grafo.existeArista(2, 4));
	}

	@Test
	public void eliminarAristaDosVecesTest() {
		Grafo grafo = new Grafo(5);
		grafo.eliminarArista(2, 3);
		grafo.eliminarArista(2, 3);

		assertFalse(grafo.existeArista(2, 3));
	}

	@Test
	public void grafoVacioTest() {
		Grafo grafo = new Grafo(0);

		assertEquals(0, grafo.tamanoDeGrafo());
	}

	@Test
	public void tamanoGrafoTest() {
		Grafo grafo = new Grafo(5);

		assertEquals(5, grafo.tamanoDeGrafo());
	}

	@Test
	public void cantidadAristasValidaTest() {
		Grafo grafo = new Grafo(4);
		grafo.agregarArista(0, 1, 1);
		grafo.agregarArista(1, 2, 1);
		grafo.agregarArista(2, 3, 1);

		assertEquals(3, grafo.cantidadAristas());
	}

	@Test
	public void vecinosTests() {
		Grafo grafo = new Grafo(4);
		grafo.agregarArista(0, 1, 1);
		grafo.agregarArista(0, 2, 1);
		grafo.agregarArista(0, 3, 1);

		Set<Integer> conjuntoEsperado = new HashSet<Integer>();
		conjuntoEsperado.add(1);
		conjuntoEsperado.add(2);
		conjuntoEsperado.add(3);

		assertEquals(conjuntoEsperado, grafo.vecinos(0));
	}

	@Test
	public void conjuntoDeVerticesConexoTest() {
		Grafo grafo = new Grafo(4);
		grafo.agregarArista(0, 1, 1);
		grafo.agregarArista(1, 2, 1);
		grafo.agregarArista(2, 3, 1);

		Set<Integer> conjuntoEsperado = new HashSet<Integer>();
		conjuntoEsperado.add(0);
		conjuntoEsperado.add(1);
		conjuntoEsperado.add(2);
		conjuntoEsperado.add(3);

		assertEquals(conjuntoEsperado, grafo.conjuntoDeVertices());
	}

	@Test
	public void conjuntoDeVerticesDisconexoTest() {
		Grafo grafo = new Grafo(5);
		grafo.agregarArista(0, 1, 1);
		grafo.agregarArista(1, 2, 1);
		grafo.agregarArista(3, 4, 1);

		Set<Integer> conjuntoEsperado = new HashSet<Integer>();
		conjuntoEsperado.add(0);
		conjuntoEsperado.add(1);
		conjuntoEsperado.add(2);
		conjuntoEsperado.add(3);
		conjuntoEsperado.add(4);

		assertEquals(conjuntoEsperado, grafo.conjuntoDeVertices());
	}

	@Test
	public void eliminarPesosDeArista() {
		Grafo grafo = new Grafo(5);
		grafo.agregarArista(1, 2, 15);

		assertEquals(1, grafo.getPesosDeAristas().size());
		grafo.eliminarArista(1, 2);

		assertEquals(0, grafo.getPesosDeAristas().size());
	}

	@Test
	public void existePesoAristaTest() {
		Grafo grafo = new Grafo(2);
		grafo.agregarArista(0, 1, 15);

		assertEquals(15, grafo.getPesoArista(0, 1));
	}
}