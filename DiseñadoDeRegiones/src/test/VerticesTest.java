package test;

import static org.junit.Assert.*;
import java.util.Set;
import org.junit.Test;

import logica.Grafo;

public class VerticesTest {

	@Test(expected = IllegalArgumentException.class)
	public void verticeNegativoTest() {
		Grafo g = new Grafo(5);
		g.vecinos(-1);
	}

	@Test(expected = IllegalArgumentException.class)
	public void verticeExcedidoTest() {
		Grafo g = new Grafo(5);
		g.vecinos(6);
	}

	@Test
	public void verticesAisladosTest() {
		Grafo g = new Grafo(5);
		assertEquals(0, g.vecinos(2).size());
	}

	@Test
	public void verticeNormalTest() {
		Grafo grafo = new Grafo(5);
		grafo.agregarArista(1, 3, 10);
		grafo.agregarArista(2, 3, 20);
		grafo.agregarArista(1, 4, 30);

		int[] esperado = { 1, 2 };
		assertIguales(esperado, grafo.vecinos(3));
	}
	
	@Test
	public void verticeUniversalTest() {
		Grafo grafo = new Grafo(4);
		grafo.agregarArista(1, 0, 7);
		grafo.agregarArista(1, 2, 5);
		grafo.agregarArista(1, 3, 14);

		int[] esperado = { 0, 2, 3 };
		assertIguales(esperado, grafo.vecinos(1));
	}

	private static void assertIguales(int[] esperado, Set<Integer> obtenido) {
		for (int i = 0; i < esperado.length; i++)
			assertTrue(obtenido.contains(esperado[i]));
	}
}