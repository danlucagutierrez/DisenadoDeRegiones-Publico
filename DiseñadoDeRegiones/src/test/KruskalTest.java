package test;

import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;

import logica.AGM;
import logica.Grafo;

public class KruskalTest {

	private Grafo g;
	private AGM grafoArgentina;
	private Grafo grafoArgentinaMinima;

	@Before
	public void grafoArgentina() {

		g = new Grafo(24);
		g.agregarArista(0, 1, 1); // 0: Tierra del Fuego - 1: Santa Cruz.
		g.agregarArista(1, 2, 2); // 1: Santa Cruz - 2: Chubut.
		g.agregarArista(2, 3, 3); // 2: Chubut - 3: Rio Negro.
		g.agregarArista(3, 4, 4); // 3: Rio Negro - 4: Nequ�n.
		g.agregarArista(3, 5, 5); // 3: Rio Negro - 5: Mendoza.
		g.agregarArista(3, 6, 100); // 3: Rio Negro - 6: La Pampa.
		g.agregarArista(3, 7, 7); // 3: Rio Negro - 7: Buenos aires.
		g.agregarArista(4, 5, 8); // 4: Neuqu�n - 5: Mendoza.
		g.agregarArista(4, 6, 9); // 4: Neuqu�n - 6: La Pampa.
		g.agregarArista(5, 6, 10); // 5: Mendoza - 6: La Pampa.
		g.agregarArista(5, 8, 11); // 5: Mendoza - 8: San Luis.
		g.agregarArista(5, 9, 12); // 5: Mendoza - 9: San Juan.
		g.agregarArista(6, 7, 13); // 6: La Pampa - 7: Buenos Aires.
		g.agregarArista(6, 8, 14); // 6: La Pampa - 8: San Luis.
		g.agregarArista(6, 10, 15); // 6: La Pampa - 10: Cordoba.
		g.agregarArista(7, 11, 16); // 7: Buenos Aires - 11: CABA.
		g.agregarArista(7, 10, 17); // 7: Buenos Aires - 10: Cordoba.
		g.agregarArista(7, 12, 18); // 7: Buenos Aires - 12: Santa Fe.
		g.agregarArista(7, 13, 101); // 7: Buenos Aires - 13: Entre R�os.
		g.agregarArista(8, 9, 19); // 8: San Luis - 9: San Juan.
		g.agregarArista(8, 14, 20); // 8: San Luis - 14: La Rioja.
		g.agregarArista(8, 10, 21); // 8: San Luis - 10: Cordoba.
		g.agregarArista(9, 14, 22); // 9: San Juan - 14: La Rioja.
		g.agregarArista(10, 14, 23); // 10: Cordoba - 14: La Rioja.
		g.agregarArista(10, 15, 24); // 10: Cordoba - 15: Catamarca.
		g.agregarArista(10, 16, 25); // 10: Cordoba - 16: Santiago del Estero.
		g.agregarArista(10, 12, 102); // 10: Cordoba - 12: Santa Fe.
		g.agregarArista(12, 16, 26); // 12: Santa Fe - 16: Santiago del Estero.
		g.agregarArista(12, 13, 27); // 12: Santa Fe - 13: Entre R�os.
		g.agregarArista(12, 17, 28); // 12: Santa Fe - 17: Chaco.
		g.agregarArista(12, 18, 29); // 12: Santa Fe - 18: Corrientes.
		g.agregarArista(14, 15, 30); // 14: La Rioja - 15: Catamarca.
		g.agregarArista(15, 19, 31); // 15: Catamarca - 19: Tucuman.
		g.agregarArista(15, 16, 32); // 15: Catamarca - 16: Santiago del Estero.
		g.agregarArista(15, 20, 33); // 15: Catamarca - 20: Salta.
		g.agregarArista(16, 19, 34); // 16: Santiago del Estero - 19: Tucuman.
		g.agregarArista(16, 20, 35); // 16: Santiago del Estero - 20: Salta.
		g.agregarArista(16, 17, 103); // 16: Santiago del Estero - 17: Chaco.
		g.agregarArista(17, 20, 36); // 17: Chaco - 20: Salta.
		g.agregarArista(17, 21, 37); // 17: Chaco - 21: Formosa.
		g.agregarArista(17, 18, 38); // 17: Chaco - 18: Corrientes.
		g.agregarArista(18, 22, 39); // 18: Corrientes - 22: Misiones.
		g.agregarArista(19, 20, 40); // 19: Tucuman - 20: Salta.
		g.agregarArista(20, 23, 41); // 20: Salta - 23: Jujuy.
		g.agregarArista(20, 21, 42); // 20: Salta - 21: Formosa.

		grafoArgentina = new AGM();
		grafoArgentinaMinima = grafoArgentina.kruskal(g);	
	}

	@Test
	public void aristaExistenteTest() {

		assertTrue(grafoArgentinaMinima.existeArista(3, 4));
	}

	@Test
	public void aristaOpuestaExistenteTest() {

		assertTrue(grafoArgentinaMinima.existeArista(4, 3));
	}

	@Test
	public void noExisteAristaTest() {

		assertFalse(grafoArgentinaMinima.existeArista(15, 23));
	}

	@Test
	public void noExisteAristaOpuestaTest() {

		assertFalse(grafoArgentinaMinima.existeArista(23, 15));
	}

	@Test
	public void cantidadAristasTest() {

		assertEquals(23, grafoArgentinaMinima.cantidadAristas());
	}

	@Test
	public void cantidadVertices() {

		assertEquals(24, grafoArgentinaMinima.tamanoDeGrafo());
	}

	@Test
	public void kruskalTest() {

		Grafo g = new Grafo(9);
		g.agregarArista(0, 1, 4);
		g.agregarArista(1, 2, 8);
		g.agregarArista(2, 3, 6);
		g.agregarArista(3, 4, 9);
		g.agregarArista(4, 5, 10);
		g.agregarArista(5, 6, 3);
		g.agregarArista(6, 7, 1);
		g.agregarArista(0, 7, 8);
		g.agregarArista(1, 7, 12);
		g.agregarArista(2, 5, 4);
		g.agregarArista(2, 8, 3);
		g.agregarArista(3, 5, 13);
		g.agregarArista(6, 8, 5);
		g.agregarArista(7, 8, 6);
		
		AGM k = new AGM();
		Grafo arbolMinimo = k.kruskal(g);
		assertTrue(arbolMinimo.existeArista(3, 4));
	}
}