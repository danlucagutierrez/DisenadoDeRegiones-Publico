package logica;

public class AGM {
	private int[] listaDePadres;
	private int[] tamanoDeComponenteConexa;
	private static int auxvA;
	private static int auxvB;
	private Grafo arbol;

	public Grafo kruskal(Grafo g) {
		if (esConexo(g))
		{
			listaDePadres = new int[g.tamanoDeGrafo()];
			tamanoDeComponenteConexa = new int[g.tamanoDeGrafo()];
	
			for (int i = 0; i < g.tamanoDeGrafo(); i++) {
				listaDePadres[i] = i;
				tamanoDeComponenteConexa[i] = 0;
			}
	
			arbol = new Grafo(g.tamanoDeGrafo());
	
			int contador = 0;
			int cantidadDeAristas = g.cantidadAristas();
			while (contador < cantidadDeAristas) {
				int pesoMinimo = BuscarPeso.optimo(g, "minimo");
				auxvA = BuscarPeso.getVerticeA();
				auxvB = BuscarPeso.getVerticeB();
				if (!estanEnMismaCompononeteConexa(auxvA, auxvB)) {
					union(auxvA, auxvB);
					arbol.agregarArista(auxvA, auxvB, pesoMinimo);
					System.out.println(auxvA + "," + auxvB + "," + pesoMinimo);
					g.eliminarArista(auxvA, auxvB);
					contador++;
				} else {
					g.eliminarArista(auxvA, auxvB);
				}
			}
			return arbol;
		}
		else
			throw new IllegalArgumentException("el grafo no posee AGM porque es un grafo disconexo");
	}

	public boolean estanEnMismaCompononeteConexa(int i, int j) {
		return buscarRaiz(i) == buscarRaiz(j);
	}

	public void union(int i, int j) {
		int raiz_i = buscarRaiz(i);
		int raiz_j = buscarRaiz(j);
		if (tamanoDeComponenteConexa[raiz_i] >= tamanoDeComponenteConexa[raiz_j])
			listaDePadres[raiz_j] = raiz_i;
		else
			listaDePadres[raiz_i] = raiz_j;
	}

	public int buscarRaiz(int vertice) {
		int cantDeVerticesRecorridos = 1;
		int verticeIncial = vertice;
		while (listaDePadres[vertice] != vertice) {
			vertice = listaDePadres[vertice];
			cantDeVerticesRecorridos++;
		}
		listaDePadres[verticeIncial] = vertice;
		tamanoDeComponenteConexa[vertice] = cantDeVerticesRecorridos;
		return vertice;
	}
	
	public boolean esConexo(Grafo grafo) {
		if (BFS.alcanzables(grafo, 0).size() == grafo.conjuntoDeVertices().size()) {
			return true;
		}
		return false;
	}
}