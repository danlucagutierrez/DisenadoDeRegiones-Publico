package logica;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class BFS {
	public static Set<Integer> alcanzables(Grafo grafo, int verticeOrigen) {
		Set<Integer> marcados = new HashSet<Integer>();
		ArrayList<Integer> pendientes = new ArrayList<Integer>();
		pendientes.add(verticeOrigen);
		while (pendientes.size() != 0) {
			int vertceActual = pendientes.get(0);
			marcados.add(vertceActual);
			pendientes.remove(0);

			for (Integer esteVecino : grafo.vecinos(vertceActual))
				if (marcados.contains(esteVecino) == false)
					pendientes.add(esteVecino);
		}
		return marcados;
	}
}