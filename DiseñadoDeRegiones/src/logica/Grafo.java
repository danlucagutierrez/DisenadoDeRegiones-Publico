package logica;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

public class Grafo {
	private ArrayList<HashSet<Integer>> listaDeVertices;
	private HashMap<String, Integer> pesosDeAristas;

	public Grafo(int cantDeVertices) {
		cantidadDeVerticesValida(cantDeVertices);

		listaDeVertices = new ArrayList<HashSet<Integer>>();
		for (int verticescreados = 0; verticescreados < cantDeVertices; verticescreados++)
			listaDeVertices.add(new HashSet<Integer>());

		pesosDeAristas = new HashMap<String, Integer>();
	}

	public void agregarArista(int verticeA, int verticeB, int pesoDeArista) {
		verticeValido(verticeA);
		verticeValido(verticeB);
		verificarDistintos(verticeA, verticeB);
		verificarPeso(pesoDeArista);
		verificarAristaOpuesta(verticeA, verticeB);

		listaDeVertices.get(verticeA).add(verticeB);
		listaDeVertices.get(verticeB).add(verticeA);
		String vector = formarVector(verticeA, verticeB);
		pesosDeAristas.put(vector, pesoDeArista);
	}

	public void eliminarArista(int verticeA, int verticeB) {
		verticeValido(verticeA);
		verticeValido(verticeB);
		verificarDistintos(verticeA, verticeB);

		listaDeVertices.get(verticeA).remove(verticeB);
		listaDeVertices.get(verticeB).remove(verticeA);

		String vector = formarVector(verticeA, verticeB);
		pesosDeAristas.remove(vector);
	}

	public boolean existeArista(int verticeA, int verticeB) {
		verticeValido(verticeA);
		verticeValido(verticeB);
		verificarDistintos(verticeA, verticeB);

		return listaDeVertices.get(verticeA).contains(verticeB);
	}

	public void verificarAristaOpuesta(int verticeA, int verticeB) {

		if (listaDeVertices.get(verticeB).contains(verticeA)) {
			throw new IllegalArgumentException("El grafo ya contiene esta arista: " + verticeA + "-" + verticeB);
		}
	}

	public int tamanoDeGrafo() {

		return listaDeVertices.size();
	}

	public int cantidadAristas() {

		return tamanoDeGrafo() - 1;
	}

	public Set<Integer> vecinos(int verticeA) {
		verticeValido(verticeA);

		return listaDeVertices.get(verticeA);
	}

	public Set<Integer> conjuntoDeVertices() {

		Set<Integer> vertices = new HashSet<Integer>();

		for (int i = 0; i < tamanoDeGrafo(); i++) {
			vertices.add(i);
		}
		return vertices;
	}
	
	public int getPesoArista(int vA, int vB) {

		existeArista(vA, vB);
		return pesosDeAristas.get(formarVector(vA, vB));
	}

	public String formarVector(int vA, int vB) {
		String v_A = Integer.toString(vA);
		String v_B = Integer.toString(vB);
		String vectorNuevo = v_A + "." + v_B;
		return vectorNuevo;
	}

	private void cantidadDeVerticesValida(int cantVertices) {
		if (cantVertices < 0)
			throw new IllegalArgumentException("El grafo no puede tener cantidad negativa de vertices");
	}

	private void verificarPeso(int pesoDeArista) {
		if (pesoDeArista < 0)
			throw new IllegalArgumentException("El peso de un vertice no puede ser negativo");
	}

	private void verticeValido(int i) {

		if (i < 0)
			throw new IllegalArgumentException("El vertice no puede ser negativo: " + i);

		if (i >= tamanoDeGrafo())
			throw new IllegalArgumentException("Los vertices deben estar entre 1 y |v|-1: " + i);

	}

	private void verificarDistintos(int i, int j) {
		if (i == j)
			throw new IllegalArgumentException("No se permiten loops : " + "(" + i + "," + j + ")");
	}

	public HashMap<String, Integer> getPesosDeAristas() {

		return pesosDeAristas;
	}
}