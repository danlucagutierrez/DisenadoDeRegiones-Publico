package logica;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class SepararEnRegiones 
{
	private static int auxvA;
	private static int auxvB;
	
	public static ArrayList<Set<Integer>> conexas(Grafo tree, int cantidadDeRegiones) {
		ArrayList<Set<Integer>> listaDeRegionesConexas = new ArrayList<Set<Integer>>();

		if (cantidadDeRegiones <= tree.tamanoDeGrafo()) {
			int i = 1;
			while (i < cantidadDeRegiones) {
				BuscarPeso.optimo(tree, "maximo");
				auxvA = BuscarPeso.getVerticeA();
				auxvB = BuscarPeso.getVerticeB();
				tree.eliminarArista(auxvA, auxvB);
				i++;
			}
		} else
			throw new IllegalArgumentException(
					"No se puede separar en mas regiones que la cantidad de vertices del Grafo");

		Set<Integer> todosLosVertices = new HashSet<Integer>();
		todosLosVertices = tree.conjuntoDeVertices();

		while (todosLosVertices.size() != 0) {
			int vertice = todosLosVertices.iterator().next();
			Set<Integer> conjunto = BFS.alcanzables(tree, vertice);
			listaDeRegionesConexas.add(conjunto);
			todosLosVertices.removeAll(conjunto);
		}
		return listaDeRegionesConexas;
	}
}
