package logica;

public class BuscarPeso 
{	
	private static int verticeA;
	private static int verticeB;
	
	public static int optimo(Grafo g, String extremo) {
		String primerValor = g.getPesosDeAristas().keySet().iterator().next();
		int pesoOptimo = g.getPesosDeAristas().get(primerValor);

		for (String arista : g.getPesosDeAristas().keySet()) {
			if (extremo.equals("minimo")) {
				if (pesoOptimo >= g.getPesosDeAristas().get(arista)) {
					pesoOptimo = g.getPesosDeAristas().get(arista);
					verticeA = parteEntera(arista);
					verticeB = parteDecimal(arista);
				}
			} else {
				if (pesoOptimo <= g.getPesosDeAristas().get(arista)) {
					pesoOptimo = g.getPesosDeAristas().get(arista);
					verticeA = parteEntera(arista);
					verticeB = parteDecimal(arista);
				}
			}
		}
		return pesoOptimo;
	}

	public static int parteEntera(String vector) {
		int partEntera = (int) (Double.parseDouble(vector));
		return partEntera;
	}

	public static int parteDecimal(String vector) {
		int despuesDelpunto = vector.indexOf(".") + 1;
		String pedazoDecimal = vector.substring(despuesDelpunto);
		int partDecimal = Integer.parseInt(pedazoDecimal);
		return partDecimal;
	}
	
	public static int getVerticeA() {
		return verticeA;
	}

	public static int getVerticeB() {
		return verticeB;
	}
	
}
