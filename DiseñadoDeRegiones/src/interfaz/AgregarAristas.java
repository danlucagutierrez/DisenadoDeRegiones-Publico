package interfaz;

import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.SpinnerNumberModel;
import java.awt.Font;
import javax.swing.JSpinner;
import javax.swing.JButton;
import javax.swing.SwingConstants;

import logica.AGM;
import logica.Grafo;

import java.awt.Color;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.ActionEvent;
import java.awt.SystemColor;
import java.awt.TextArea;

public class AgregarAristas {

	JFrame generadorAristas;
	static Grafo grafo;
	static String aristasAgregadas = "";
	JButton botonGenerarArista;
	int contador;
	AGM agm = new AGM();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AgregarAristas window = new AgregarAristas();
					window.generadorAristas.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public AgregarAristas() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		grafo = CrearGrafo.getGrafo();
		contador = 0;

		generadorAristas = new JFrame();
		generadorAristas.getContentPane().setBackground(SystemColor.controlHighlight);
		generadorAristas.setTitle("Dise\u00F1ador de regiones");
		generadorAristas.setResizable(false);
		generadorAristas.setBounds(100, 100, 623, 358);
		generadorAristas.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		generadorAristas.getContentPane().setLayout(null);
		generadorAristas.setLocationRelativeTo(null);

		JLabel labelVerticeA = new JLabel("Vertice A");
		labelVerticeA.setBounds(75, 108, 63, 14);
		labelVerticeA.setFont(new Font("Tahoma", Font.PLAIN, 12));
		generadorAristas.getContentPane().add(labelVerticeA);

		JLabel labelVerticeB = new JLabel("Vertice B");
		labelVerticeB.setBounds(75, 133, 63, 14);
		labelVerticeB.setFont(new Font("Tahoma", Font.PLAIN, 12));
		generadorAristas.getContentPane().add(labelVerticeB);

		JLabel TituloAristas = new JLabel("Insertar aristas");
		TituloAristas.setBounds(10, 11, 324, 41);
		TituloAristas.setForeground(Color.DARK_GRAY);
		TituloAristas.setHorizontalAlignment(SwingConstants.CENTER);
		TituloAristas.setFont(new Font("Tahoma", Font.PLAIN, 24));
		generadorAristas.getContentPane().add(TituloAristas);

		JSpinner spinnerVerticeA = new JSpinner(new SpinnerNumberModel(0, 0, grafo.tamanoDeGrafo() - 1, 1));
		spinnerVerticeA.setBounds(234, 105, 41, 20);
		spinnerVerticeA.setFont(new Font("Tahoma", Font.PLAIN, 12));
		generadorAristas.getContentPane().add(spinnerVerticeA);

		JSpinner spinnerVerticeB = new JSpinner(new SpinnerNumberModel(0, 0, grafo.tamanoDeGrafo() - 1, 1));
		spinnerVerticeB.setBounds(234, 130, 41, 20);
		spinnerVerticeB.setFont(new Font("Tahoma", Font.PLAIN, 12));
		generadorAristas.getContentPane().add(spinnerVerticeB);

		JSpinner peso = new JSpinner(new SpinnerNumberModel(0, 0, 10000, 1));
		peso.setBounds(234, 155, 41, 20);
		peso.setFont(new Font("Tahoma", Font.PLAIN, 12));
		generadorAristas.getContentPane().add(peso);

		JLabel lblPesos = new JLabel("Peso arista");
		lblPesos.setBounds(75, 158, 63, 14);
		lblPesos.setFont(new Font("Tahoma", Font.PLAIN, 12));
		generadorAristas.getContentPane().add(lblPesos);

		JLabel mensaje = new JLabel("�Error en alg�n dato!");
		mensaje.setForeground(Color.RED);
		mensaje.setBackground(Color.WHITE);
		mensaje.setHorizontalAlignment(SwingConstants.CENTER);
		mensaje.setBounds(75, 57, 200, 40);
		mensaje.setFont(new Font("Tahoma", Font.PLAIN, 14));
		generadorAristas.getContentPane().add(mensaje);
		mensaje.setVisible(false);

		TextArea textArea = new TextArea();
		textArea.setEditable(false);
		textArea.setBounds(307, 69, 253, 226);
		generadorAristas.getContentPane().add(textArea);

		JButton botonAgregarMasAristas = new JButton("Agregar arista");
		botonAgregarMasAristas.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				Integer verticeA = Integer.parseInt(spinnerVerticeA.getValue().toString());
				Integer verticeB = Integer.parseInt(spinnerVerticeB.getValue().toString());
				Integer pesoArista = Integer.parseInt(peso.getValue().toString());
				spinnerVerticeA.setValue(0);
				spinnerVerticeB.setValue(0);
				peso.setValue(0);

				try {
					grafo.agregarArista(verticeA, verticeB, pesoArista);
					aristasAgregadas = aristasAgregadas + "\nArista entre los vertices " + verticeA + " y " + verticeB
							+ ", de Peso: " + pesoArista;
					textArea.setText(aristasAgregadas);
					contador++;
					mensaje.setVisible(false);
				} catch (Exception RuntimeException) {
					mensaje.setVisible(true);
				}

				if (agm.esConexo(grafo)) {
					botonGenerarArista.setVisible(true);
				}
			}
		});
		botonAgregarMasAristas.setFont(new Font("Tahoma", Font.PLAIN, 12));
		botonAgregarMasAristas.setBounds(75, 194, 200, 45);
		generadorAristas.getContentPane().add(botonAgregarMasAristas);

		botonGenerarArista = new JButton("Continuar");
		botonGenerarArista.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				generadorAristas.setVisible(false);
				CrearAGMxRegiones ventana = new CrearAGMxRegiones();
				ventana.generadorAGMxRegiones.setVisible(true);
			}
		});
		botonGenerarArista.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});

		botonGenerarArista.addMouseListener(new MouseAdapter() {
			public void mouseEntered(MouseEvent e) {

				botonGenerarArista.setText("Por comenzar...");
			}

			public void mouseExited(MouseEvent e) {

				botonGenerarArista.setText("Continuar");
			}
		});

		botonGenerarArista.setBounds(75, 250, 200, 45);
		botonGenerarArista.setFont(new Font("Tahoma", Font.PLAIN, 12));
		generadorAristas.getContentPane().add(botonGenerarArista);
		botonGenerarArista.setVisible(false);

		JLabel lblAristasInsertadas = new JLabel("Aristas insertadas");
		lblAristasInsertadas.setHorizontalAlignment(SwingConstants.CENTER);
		lblAristasInsertadas.setForeground(Color.DARK_GRAY);
		lblAristasInsertadas.setFont(new Font("Tahoma", Font.PLAIN, 24));
		lblAristasInsertadas.setBounds(268, 11, 324, 41);
		generadorAristas.getContentPane().add(lblAristasInsertadas);

	}

	public static Grafo getGrafoConAristas() {

		return grafo;
	}
}