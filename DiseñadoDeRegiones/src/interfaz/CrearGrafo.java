package interfaz;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.SpinnerNumberModel;
import javax.swing.JLabel;
import javax.swing.JSpinner;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.ActionEvent;
import java.awt.Font;
import javax.swing.SwingConstants;

import logica.Grafo;

import java.awt.Color;
import java.awt.SystemColor;

public class CrearGrafo {

	JFrame generadorGrafo;
	static Grafo grafo;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					CrearGrafo window = new CrearGrafo();
					window.generadorGrafo.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public CrearGrafo() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		generadorGrafo = new JFrame();
		generadorGrafo.getContentPane().setBackground(SystemColor.controlHighlight);
		generadorGrafo.setResizable(false);
		generadorGrafo.setTitle("Dise\u00F1ador de regiones");
		generadorGrafo.setBounds(100, 100, 350, 350);
		generadorGrafo.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		generadorGrafo.getContentPane().setLayout(null);
		generadorGrafo.setLocationRelativeTo(null);

		JLabel labelVertices = new JLabel("Cantidad de vertices");
		labelVertices.setFont(new Font("Tahoma", Font.PLAIN, 12));
		labelVertices.setBounds(75, 118, 120, 42);
		generadorGrafo.getContentPane().add(labelVertices);

		JSpinner spinnerVertices = new JSpinner(new SpinnerNumberModel(2, 2, 10000, 1));
		spinnerVertices.setFont(new Font("Tahoma", Font.PLAIN, 12));
		spinnerVertices.setBounds(231, 129, 44, 22);
		generadorGrafo.getContentPane().add(spinnerVertices);

		JButton botonGenerarGrafo = new JButton("Generar grafo");
		botonGenerarGrafo.setFont(new Font("Tahoma", Font.PLAIN, 12));
		botonGenerarGrafo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				Integer cantidadVertices = Integer.parseInt(spinnerVertices.getValue().toString());
				grafo = new Grafo(cantidadVertices);
				generadorGrafo.setVisible(false);
				AgregarAristas ventana = new AgregarAristas();
				ventana.generadorAristas.setVisible(true);
			}
		});
		botonGenerarGrafo.addMouseListener(new MouseAdapter() {
			public void mouseEntered(MouseEvent e) {

				botonGenerarGrafo.setText("Por comenzar...");
			}

			public void mouseExited(MouseEvent e) {

				botonGenerarGrafo.setText("Crear grafo");
			}
		});

		botonGenerarGrafo.setBounds(75, 225, 200, 45);
		generadorGrafo.getContentPane().add(botonGenerarGrafo);

		JLabel TituloGrafo = new JLabel("Crear grafo");
		TituloGrafo.setHorizontalAlignment(SwingConstants.CENTER);
		TituloGrafo.setForeground(Color.DARK_GRAY);
		TituloGrafo.setFont(new Font("Tahoma", Font.PLAIN, 24));
		TituloGrafo.setBounds(10, 11, 324, 42);
		generadorGrafo.getContentPane().add(TituloGrafo);
	}

	public static Grafo getGrafo() {

		return grafo;
	}
}