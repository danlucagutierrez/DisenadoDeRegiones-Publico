package interfaz;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.SystemColor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Set;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingConstants;

import logica.AGM;
import logica.Grafo;
import logica.SepararEnRegiones;

import javax.swing.JTextArea;

public class CrearAGMxRegiones {

	JFrame generadorAGMxRegiones;
	JLabel subTitulo;

	Grafo grafoConAristas;
	Grafo agm;
	AGM algoritmo;

	JButton botonGenerarAGMxRegiones;
	int cantidadDeRegiones;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					CrearAGMxRegiones window = new CrearAGMxRegiones();
					window.generadorAGMxRegiones.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public CrearAGMxRegiones() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		grafoConAristas = AgregarAristas.getGrafoConAristas();
		AGM algoritmo = new AGM();
		
		generadorAGMxRegiones = new JFrame();
		generadorAGMxRegiones.getContentPane().setFont(new Font("Tahoma", Font.PLAIN, 12));
		generadorAGMxRegiones.getContentPane().setBackground(SystemColor.controlHighlight);
		generadorAGMxRegiones.setTitle("Dise\u00F1ador de regiones");
		generadorAGMxRegiones.setResizable(false);
		generadorAGMxRegiones.setBounds(100, 100, 502, 498);
		generadorAGMxRegiones.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		generadorAGMxRegiones.getContentPane().setLayout(null);
		generadorAGMxRegiones.setLocationRelativeTo(null);

		JLabel TituloAGMxRegiones = new JLabel("Crear AGM y mostrar regiones");
		TituloAGMxRegiones.setBounds(10, 11, 474, 76);
		TituloAGMxRegiones.setForeground(Color.DARK_GRAY);
		TituloAGMxRegiones.setHorizontalAlignment(SwingConstants.CENTER);
		TituloAGMxRegiones.setFont(new Font("Tahoma", Font.PLAIN, 24));
		generadorAGMxRegiones.getContentPane().add(TituloAGMxRegiones);

		JSpinner regiones = new JSpinner(new SpinnerNumberModel(1, 1, 10000, 1));
		regiones.setBounds(299, 98, 41, 20);
		regiones.setFont(new Font("Tahoma", Font.PLAIN, 12));
		generadorAGMxRegiones.getContentPane().add(regiones);

		JLabel lblCantidadRegiones = new JLabel("Cantidad de regiones");
		lblCantidadRegiones.setBounds(140, 98, 115, 21);
		lblCantidadRegiones.setFont(new Font("Tahoma", Font.PLAIN, 12));
		generadorAGMxRegiones.getContentPane().add(lblCantidadRegiones);

		JTextArea textAreaConjuntos = new JTextArea();
		textAreaConjuntos.setFont(new Font("Tahoma", Font.PLAIN, 18));
		textAreaConjuntos.setTabSize(15);
		textAreaConjuntos.setEditable(false);
		textAreaConjuntos.setBounds(20, 228, 452, 204);
		generadorAGMxRegiones.getContentPane().add(textAreaConjuntos);

		subTitulo = new JLabel("Se generaron las siguientes regiones:");
		subTitulo.setHorizontalAlignment(SwingConstants.CENTER);
		subTitulo.setForeground(Color.DARK_GRAY);
		subTitulo.setFont(new Font("Tahoma", Font.PLAIN, 18));
		subTitulo.setBounds(10, 185, 474, 32);
		generadorAGMxRegiones.getContentPane().add(subTitulo);
		subTitulo.setVisible(false);

		botonGenerarAGMxRegiones = new JButton("Crear y mostrar");
		botonGenerarAGMxRegiones.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				int cantidadDeRegiones = Integer.parseInt(regiones.getValue().toString());
				
				agm = algoritmo.kruskal(grafoConAristas);
				
				try {
				ArrayList<Set<Integer>> conjuntoDeRegiones = SepararEnRegiones.conexas(agm, cantidadDeRegiones);
				
				String auxiliar = "";
				for (Set<Integer> elemento : conjuntoDeRegiones) {
					auxiliar = auxiliar + "\nRegion: " + elemento.toString();
				}

				textAreaConjuntos.setText(auxiliar);

				} catch (Exception IllegalArgumentException) {
					textAreaConjuntos.setText("\nNo se pueden generar regiones.");
					textAreaConjuntos.setForeground(Color.RED);
				}
				textAreaConjuntos.setVisible(true);
				botonGenerarAGMxRegiones.setText("Proceso completado");
				subTitulo.setVisible(true);
				botonGenerarAGMxRegiones.setVisible(false);
			}
		});

		botonGenerarAGMxRegiones.addMouseListener(new MouseAdapter() {
			public void mouseEntered(MouseEvent e) {

				botonGenerarAGMxRegiones.setText("Por comenzar...");
			}

			public void mouseExited(MouseEvent e) {

				botonGenerarAGMxRegiones.setText("Crear y mostrar");
			}
		});

		botonGenerarAGMxRegiones.setBounds(140, 129, 200, 45);
		botonGenerarAGMxRegiones.setFont(new Font("Tahoma", Font.PLAIN, 12));
		generadorAGMxRegiones.getContentPane().add(botonGenerarAGMxRegiones);
	}
}